#!/bin/bash

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S linux-headers avahi xdg-user-dirs xdg-utils
pacman -S mtools dosfstools ntfs-3g
pacman -S networkmanager network-manager-applet wpa_supplicant nfs-utils inetutils bind # bind apparently replaces dnsutils
pacman -S bridge-utils dnsmasq iptables-nft ipset firewalld nss-mdns
pacman -S sof-firmware alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bluez bluez-utils
pacman -S acpi tlp # acpid acpi_call
pacman -S bash-completion openssh rsync
# pacman -S cups hplip

# REMOVED: netcat flatpak os-prober terminus-font dialog

# VIRTUAL MACHINES
# pacman -S virt-manager qemu qemu-arch-extra edk2-ovmf vde2

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

# ENABLE SERVICES
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable firewalld
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
# systemctl enable acpid # We'll try to install it later if the power buttons don't end up working.
# systemctl enable libvirtd
# systemctl enable cups.service
