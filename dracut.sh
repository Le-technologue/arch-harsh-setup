#!/usr/bin/env sh

mv dracut-* /usr/local/bin
mkdir -pv /etc/pacman.d/hooks
mv *.hook /etc/pacman.d/hooks
