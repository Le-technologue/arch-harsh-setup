#!/bin/bash

mount /dev/vg0/root /mnt

mkdir -pv /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot

mkdir -pv /mnt/home
mount /dev/vg0/home /mnt/home
