#!/bin/bash

echo "Formatting the EFI Partition as FAT32."
mkfs.fat -F 32 /dev/nvme0n1p1
echo "Formatting the LUKS container as BTRFS."
mkfs.btrfs -f -L btrfs-root /dev/mapper/crypt

echo "Creating BTRFS subvolumes."
mount /dev/mapper/crypt /mnt

btrfs subvolume create /mnt/@
btrfs su cr /mnt/@swap
btrfs su cr /mnt/@home

btrfs su cr /mnt/@snapshots

btrfs su cr /mnt/@cache
btrfs su cr /mnt/@tmp
btrfs su cr /mnt/@log

btrfs su cr /mnt/@libvirt

btrfs filesystem mkswapfile --size 16g --uuid clear /mnt/@swap/swapfile

umount /mnt
