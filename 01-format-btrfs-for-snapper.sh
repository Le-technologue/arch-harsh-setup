#!/bin/bash

# FORMATTING
#
echo "Formatting the EFI Partition as FAT32."
mkfs.fat -F 32 /dev/nvme0n1p1
echo "Formatting the LUKS container as BTRFS."
mkfs.btrfs -f -L btrfs-root /dev/mapper/crypt

echo "Creating BTRFS subvolumes."
mount -o clear_cache,nospace_cache /dev/mapper/crypt /mnt

# SUBVOLUMES CREATION
#
# MAIN SUBVOLUMES
#
# Root subvolume
btrfs subvolume create /mnt/@
# Home subvolumes
btrfs su cr /mnt/@home
btrfs su cr /mnt/@root
# Swap subvolume
btrfs su cr /mnt/@swap
# Swapfile creation
btrfs filesystem mkswapfile --size 16g --uuid clear /mnt/@swap/swapfile

# SUB-ROOT SUBVOLUMES
#
# Snapshots subvolumes
btrfs su cr /mnt/@/.snapshots
# First snapshot, will be set to default, thus contains our first filesystem
mkdir -pv /mnt/@/.snapshots/1
btrfs su cr /mnt/@/.snapshots/1/snapshot
#
# System subvolumes
#
# btrfs su cr /mnt/@/boot
btrfs su cr /mnt/@/tmp
# /var subvols
mkdir -pv /mnt/@/var
btrfs su cr /mnt/@/var/log
btrfs su cr /mnt/@/var/tmp
btrfs su cr /mnt/@/var/cache
btrfs su cr /mnt/@/var/crash
btrfs su cr /mnt/@/var/spool
# Probably useful to exclude when you make a server... we'll see if it was justified.
btrfs su cr /mnt/@/srv

# Excluded these for now, what if I also want to rollback my third party apps ?
# btrfs su cr /mnt/@/opt
# mkdir -pv /mnt/@/usr
# btrfs su cr /mnt/@/usr/local

# Disabling COW on var
chattr +C /mnt/@/var/log
chattr +C /mnt/@/var/tmp
chattr +C /mnt/@/var/cache
chattr +C /mnt/@/var/crash
chattr +C /mnt/@/var/spool

# Will tend to those later when we'll be setting up virtual machines
#
# btrfs su cr /mnt/@/var_lib_libvirt_images
# btrfs su cr /mnt/@/var_lib_machines
# btrfs su cr /mnt/@/var_lib_gdm
# btrfs su cr /mnt/@/var_lib_AccountsService
#
# Don't know what this is for
# btrfs su cr /mnt/@/cryptkey

# Set the default BTRFS Subvol to Snapshot 1 before pacstrapping
btrfs subvolume set-default "$(btrfs subvolume list /mnt | grep "@/.snapshots/1/snapshot" | grep -oP '(?<=ID )[0-9]+')" /mnt btrfs su cr /mnt/@swap

umount /mnt
