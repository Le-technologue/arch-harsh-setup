#!/bin/bash

# BOOT MANAGER
pacman -S efibootmgr

# Uncomment if using GRUB
pacman -S grub
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi
#
# ADD THE FOLLOWING OPTIONS TO /etc/default/grub next to GRUB_CMDLINE_LINUX_DEFAULT
echo "cryptdevice=UUID=e489120f-f367-40ba-8424-f4a927eb2479:root root=LABEL=btrfs-root rootflags=subvol=@" >> /etc/default/grub
vim /etc/default/grub
#
grub-mkconfig -o /boot/grub/grub.cfg
#
# Should I add this to /etc/crypttab ? apparently it boots fine without it when using GRUB
# crypt	UUID=e489120f-f367-40ba-8424-f4a927eb2479	none
