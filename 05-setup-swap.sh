#!/bin/bash

echo "# Swapfile" >> /etc/fstab
echo "/swap/swapfile	none	swap	defaults	0 0" >> /etc/fstab

swapon /swap/swapfile
