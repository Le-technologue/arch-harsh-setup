#!/usr/bin/env sh

useradd -m technologue

passwd technologue

pacman -S sudo

echo "technologue ALL=(ALL) ALL" >> /etc/sudoers.d/technologue

echo "---------------------------------------------------"
echo "You should now reboot and log in with your new user"
echo "---------------------------------------------------"
