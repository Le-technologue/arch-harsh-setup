#!/usr/bin/env sh

sudo sed -i 's/#Color/Color\nILoveCandy/g' /etc/pacman.conf

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

alias yay=paru
