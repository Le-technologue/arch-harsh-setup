#!/bin/bash

CRYPT_DEV="/dev/mapper/crypt"

BTRFS_BASE_MNT_OPTIONS="rw,noatime,compress=zstd,space_cache=v2"
BTRFS_OPTI_MNT_OPTIONS="rw,noatime,compress-force=zstd:2,space_cache=v2"

BTRFS_MNT_OPTIONS=$BTRFS_BASE_MNT_OPTIONS

# MAIN SUBVOLUMES & BOOT PARTITION
#
# Root subvolume
mount -o "$BTRFS_MNT_OPTIONS,subvol=@" $CRYPT_DEV /mnt
# Home subvolumes
mkdir -pv /mnt/home
mount -o "$BTRFS_MNT_OPTIONS,subvol=@home" $CRYPT_DEV /mnt/home
mkdir -pv /mnt/root
mount -o "$BTRFS_MNT_OPTIONS,subvol=@root" $CRYPT_DEV /mnt/root
# Swap subvolume
# mkdir -pv /mnt/swap
# mount -o subvol=@swap $BTRFS_DEV /mnt/swap
# mount -o swap @swap/swapfile
# swapon /swap/swapfile

# SUB-ROOT SUBVOLUMES
#
# Snapshots subvolumes
mkdir -pv /mnt/.snapshots
mount -o "$BTRFS_MNT_OPTIONS,subvol=@/.snapshots" $CRYPT_DEV /mnt/.snapshots
#
# System subvolumes
#
# Boot partition
mkdir -pv /mnt/boot
mount -o subvol=@/boot /dev/nvme0n1p1 /mnt/boot
#
mkdir -pv /mnt/tmp
mount -o "$BTRFS_MNT_OPTIONS,subvol=@/tmp" $CRYPT_DEV /mnt/tmp

# /var subvols
mkdir -pv /mnt/var/log
mount -o "$BTRFS_MNT_OPTIONS,nodatacow,subvol=@/var/log" $CRYPT_DEV /mnt/var/log
mkdir -pv /mnt/var/tmp
mount -o "$BTRFS_MNT_OPTIONS,nodatacow,subvol=@/var/tmp" $CRYPT_DEV /mnt/var/tmp
mkdir -pv /mnt/var/cache
mount -o "$BTRFS_MNT_OPTIONS,nodatacow,subvol=@/var/cache" $CRYPT_DEV /mnt/var/cache
mkdir -pv /mnt/var/spool
mount -o "$BTRFS_MNT_OPTIONS,nodatacow,subvol=@/var/spool" $CRYPT_DEV /mnt/var/spool
# Probably useful to exclude when you make a server... we'll see if it was justified.
mkdir -pv /mnt/srv
mount -o "$BTRFS_MNT_OPTIONS,subvol=@/srv" $CRYPT_DEV /mnt/srv

# Will tend to those later when we'll be setting up virtual machines
#
# mkdir -pv /mnt/var/lib/libvirt
# mount -o "$BTRFS_MNT_OPTIONS,subvol=@libvirt" $CRYPT_DEV /mnt/var/lib/libvirt
