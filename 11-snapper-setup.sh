#!/usr/bin/env sh

sudo pacman -S snapper snap-pac
sudo umount /.snapshots
sudo rm -rf /.snapshots
