#!/bin/bash

set -x

CWD_FILES=$(ls)
echo $CWD_FILES

for FILE in 00*
do
	mv "$FILE" "${FILE/00/}"
done
