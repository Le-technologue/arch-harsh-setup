#!/bin/bash
#

echo "--------------------------------------------------------------------------------"
echo "When vim opens, add "encrypt" to HOOKS=() after "block" and before "filesystems""
echo "--------------------------------------------------------------------------------"

pacman -S mkinitcpio

vim /etc/mkinitcpio.conf

# sed -i 's/BINARIES=()/BINARIES=(btrfs)/g' /etc/mkinitcpio.conf
# sed -i 's/MODULES=()/MODULES=(vmd)/g' /etc/mkinitcpio.conf

mkinitcpio -p linux
