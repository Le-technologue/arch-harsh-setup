#!/bin/bash

mkfs.fat -F 32 /dev/nvme0n1p1
mkfs.ext4 /dev/vg0/root
mkfs.ext4 /dev/vg0/home

swapoff /dev/vg0/swap
swapon /dev/vg0/swap
