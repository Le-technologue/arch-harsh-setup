#!/bin/bash

pacstrap -Ki /mnt base base-devel linux linux-firmware intel-ucode binutils reflector dhcpcd git vim man-db fish mkinitcpio btrfs-progs #lvm2

genfstab -U /mnt >> /mnt/etc/fstab

sed -i 's/subvolid=.*,//' /mnt/etc/fstab

echo "FSTAB GENERATED :"
echo "-----------------"
cat /mnt/etc/fstab
echo "-----------------"

mkdir -pv /mnt/install-scripts
cp /root/usb/* /mnt/install-scripts

echo "CHROOTING INTO BOOTSTRAPPED INSTALL"
arch-chroot /mnt fish
