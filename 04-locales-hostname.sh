#!/bin/bash

echo "SET ROOT PASSWORD"
passwd

ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc
sed -i '171s/.//' /etc/locale.gen # uncomment en_US locale
# sed -i '242s/.//' /etc/locale.gen # uncomment fr_FR locale
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=us" >> /etc/vconsole.conf
# echo "KEYMAP_TOGGLE=fr" >> /etc/vconsole.conf
echo "T470" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 T470.localdomain T470" >> /etc/hosts
