#!/bin/bash

BTRFS_BASE_MNT_OPTIONS="rw,noatime,compress=zstd,space_cache=v2"
BTRFS_OPTI_MNT_OPTIONS="rw,noatime,compress-force=zstd:2,space_cache=v2"

BTRFS_MNT_OPTIONS=$BTRFS_OPTI_MNT_OPTIONS

mount -o "$BTRFS_MNT_OPTIONS,subvol=@" /dev/mapper/crypt /mnt

mkdir -pv /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot
#mount -o subvol=@ /dev/nvme0n1p1 /mnt/boot
mkdir -pv /mnt/swap
mount -o subvol=@swap /dev/mapper/crypt /mnt/swap

mkdir -pv /mnt/home
mount -o "$BTRFS_MNT_OPTIONS,subvol=@home" /dev/mapper/crypt /mnt/home

#mkdir -pv /mnt/.snapshots
#mount -o "$BTRFS_OPTI_MNT_OPTIONS,subvol=@snapshots" /dev/mapper/crypt /mnt/.snapshots

mkdir -pv /mnt/var/cache
mount -o "$BTRFS_MNT_OPTIONS,subvol=@cache" /dev/mapper/crypt /mnt/var/cache
mkdir -pv /mnt/var/tmp
mount -o "$BTRFS_MNT_OPTIONS,subvol=@tmp" /dev/mapper/crypt /mnt/var/tmp
mkdir -pv /mnt/var/log
mount -o "$BTRFS_MNT_OPTIONS,subvol=@log" /dev/mapper/crypt /mnt/var/log
mkdir -pv /mnt/var/lib/libvirt
mount -o "$BTRFS_MNT_OPTIONS,subvol=@libvirt" /dev/mapper/crypt /mnt/var/lib/libvirt
